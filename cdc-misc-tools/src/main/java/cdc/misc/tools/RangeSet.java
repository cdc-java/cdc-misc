package cdc.misc.tools;

import java.util.ArrayList;
import java.util.List;

class RangeSet<T extends Comparable<? super T>> {
    private final List<Range<T>> ranges = new ArrayList<>();

    public RangeSet() {
        // Ignore
    }

    public List<Range<T>> getRanges() {
        return ranges;
    }

    public boolean contains(T value) {
        for (final Range<T> range : ranges) {
            if (range.contains(value)) {
                return true;
            }
        }
        return false;
    }
}