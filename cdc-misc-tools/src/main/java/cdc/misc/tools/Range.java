package cdc.misc.tools;

import java.util.Objects;
import java.util.function.Function;

class Range<T extends Comparable<? super T>> {
    private final T inf;
    private final T sup;

    public Range(T inf,
                 T sup) {
        this.inf = inf;
        this.sup = sup;
    }

    public Range(T value) {
        this(value, value);
    }

    public T getInf() {
        return inf;
    }

    public T getSup() {
        return sup;
    }

    public boolean isEmpty() {
        return inf.compareTo(sup) > 0;
    }

    public boolean isSingleton() {
        return inf.equals(sup);
    }

    public boolean contains(T value) {
        return inf.compareTo(value) <= 0
                && value.compareTo(sup) <= 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(inf, sup);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Range<?>)) {
            return false;
        }
        final Range<?> o = (Range<?>) other;
        return Objects.equals(inf, o.inf)
                && Objects.equals(sup, o.sup);
    }

    public static <T extends Comparable<? super T>> Range<T> parse(String s,
                                                                   Function<String, T> parser) {
        final int pos = s.indexOf('~');
        if (pos >= 0) {
            final T inf = parser.apply(s.substring(0, pos));
            final T sup = parser.apply(s.substring(pos + 1, s.length()));
            return new Range<>(inf, sup);
        } else {
            final T value = parser.apply(s);
            return new Range<>(value);
        }
    }
}